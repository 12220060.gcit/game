package main

import (
	"fmt"
	"os"
)

// declaring variables
// creating an empty 3x3 tic tac toe board using array
var board [3][3]string
var currentPlayer string = "X" //the current player, initially set to "X"

func main() {
	initBoard() //initializing the board
	playGame()  // staring the game
}

func initBoard() { //function to initialize the board with empty cells
	//looping over the each cell in the board and seeting its value to "-"
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			board[i][j] = "-"
		}
	}
}

func playGame() { // its a main game loop
	for {
		fmt.Printf("Player %s's turn\n", currentPlayer) //to print the current player's turn
		printBoard()                                    //printing the current board

		row, col := getInput() //getting the user's input for the cell they want to mark

		if board[row][col] != "-" { //checking if the cell is already marked
			fmt.Println("Invalid move, try again")
			continue
		}
		board[row][col] = currentPlayer //place the players mark on the board

		if checkWin() { //checking if the current player has won
			printBoard()
			fmt.Printf("Player %s wins!\n", currentPlayer)
			os.Exit(0) //exit the program
		}

		if checkTie() { //checking if the game is tied
			printBoard()
			fmt.Println("It's a tie!") // print the board and the tie message
			os.Exit(0)                 // exit the prigram
		}

		switchPlayer() //switching to the other player's turn
	}
}

func getInput() (int, int) { //getting the row and column number from the user
	var row, col int //declare variables for the row and column
	fmt.Print("Enter row number (0-2): ")
	fmt.Scan(&row)
	fmt.Print("Enter column number (0-2): ")
	fmt.Scan(&col)
	return row, col
}

func checkWin() bool { //checking for a win in the rows
	// Check rows
	for i := 0; i < 3; i++ {
		if board[i][0] == currentPlayer && board[i][1] == currentPlayer && board[i][2] == currentPlayer {
			return true
		}
	}

	// Check columns
	for j := 0; j < 3; j++ { //checking for a win in the columns
		if board[0][j] == currentPlayer && board[1][j] == currentPlayer && board[2][j] == currentPlayer {
			return true
		}
	}

	// Check diagonals
	//checking for the win in the diagonals
	if board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer {
		return true
	}
	if board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer {
		return true
	}

	return false
}

func checkTie() bool { //checking if all cells are filled and no player has won
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if board[i][j] == "-" {
				return false
			}
		}
	}
	return true
}

func switchPlayer() { //function switches the current player from X to O
	if currentPlayer == "X" {
		currentPlayer = "O"
	} else {
		currentPlayer = "X"
	}
}

func printBoard() { // prints the current state of board

	for i := 0; i < 3; i++ { //rows
		for j := 0; j < 3; j++ { //column
			fmt.Print(board[i][j])
			if j < 2 { // if the current column is not the end then it will print "|" between current cell and next cell.
				fmt.Print(" | ")
			}
		}
		fmt.Println()
		if i < 2 { //if the current row is not the last row, it prints a horizontal line ("-------") to separate the current row from the next row.
			fmt.Println("---------")
		}
	}
}
